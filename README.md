Import as a Project in Eclipse or Android Developer Studio,

This project use Gradle (included in Android Developer Studio or Eclipse ADT).

Dependencies:

* Android ndk.
* Android sdk.
* Eclipse ADT Plugin (Eclipse dependency)

Requisites:

* move ocr pictures (resources/ocr/) to sdcard/Pictures of the Emulator/SmartPhone or other media OS directory (use adb push or ADM File Manager).
* move tessdatam of trained languages (resources/tesseract/tessdata/) to sdcard (/sdcard/tesseract/tessdata)
* install Opencv manager apk on emulator/android device from resources/opencv Example by default: "adb install opencv OpenCV_2.4.11_Manager_2.20_armv7a-neon.apk"

+Screenshots

![image](http://www.codepool.biz/wp-content/uploads/2014/12/ocr_img.png)
![image](http://www.codepool.biz/wp-content/uploads/2014/12/do_ocr_select.png)

References:
-----------
* [tesseract-ocr][1]
* [tesseract-android-tools][2]
* [opencv][3]
* [Android ndk][4]
* [Android sdk][5]
* [Eclipse ADT][6]
* [Gradle][7]


[1]:http://code.google.com/p/tesseract-ocr/
[2]:https://code.google.com/p/tesseract-android-tools/
[3]:http://opencv.org/platforms/android.html
[4]:https://developer.android.com/tools/sdk/ndk/index.html
[5]:https://developer.android.com/sdk/index.html
[6]:http://developer.android.com/tools/sdk/eclipse-adt.html
[7]:https://gradle.org/