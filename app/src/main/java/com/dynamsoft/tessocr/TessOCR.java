package com.dynamsoft.tessocr;

import java.io.File;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

public class TessOCR {
	private TessBaseAPI mTess;
	
	public TessOCR() {
		// TODO Auto-generated constructor stub
		mTess = new TessBaseAPI();
		String datapath = Environment.getExternalStorageDirectory() + "/tesseract/";
		String language = "eng+fra+nld";
		File dir = new File(datapath + "tessdata/");
		if (!dir.exists()) 
			dir.mkdirs();
		mTess.init(datapath, language);
	}
	
	public String getOCRResult(Bitmap bitmap) {
		
		mTess.setImage(bitmap);
		String result = mTess.getUTF8Text();
        String recognizedText = result.replaceAll("[^a-zA-Z0-9]+", " ");
        recognizedText = recognizedText.trim();
        Log.v("OCR::CV", "OCRED TEXT: " + recognizedText);
        return recognizedText;
    }
	
	public void onDestroy() {
		if (mTess != null)
			mTess.end();
	}
	
}
